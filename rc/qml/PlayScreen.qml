import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0


Rectangle {

    id: initialScreen
    color: "#fff1ba"


    Button
    {
        id: backButton
        onClicked: stackView.pop();

        anchors.left: parent.left
        anchors.bottom: parent.bottom
        anchors.leftMargin: 10
        anchors.bottomMargin: 10

        width: 40
        height: 40

        contentItem: Image {
           fillMode: Image.PreserveAspectFit
           width: backButton.width
           height: backButton.height
           source: "qrc:/icons/arrow-left.png"
           clip: true
        }

        background: Rectangle {
            color: backButton.pressed ? "lightgrey" : "white"
            radius: height/3
        }
    }

    GameScene
    {
        anchors.centerIn: parent
        height: Math.min(parent.height-backButton.height*2-backButton.anchors.bottomMargin*2,
                         parent.width-backButton.width*2-backButton.anchors.leftMargin*2 )
        width: height
    }

}
