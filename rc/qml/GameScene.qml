import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

import QtQml 2.2
import QtQuick.Dialogs 1.2

Rectangle {
    id: gameScene
    color: "#e3e6fc"

    Connections
    {
        target: GameController

        onWinnerFound: {
            gameFinishedDialog.text = playerName + qsTr( " win the game. Do you want to retry?");
            gameFinishedDialog.open()
        }
        onGameFinished: {
            gameFinishedDialog.text = qsTr( "Game finished. Do you want to retry?" )
            gameFinishedDialog.open();
        }
    }

    MessageDialog
    {
        id: gameFinishedDialog
        standardButtons: MessageDialog.Yes | MessageDialog.No

        onYes: {
            GameController.restartGame();
            gameFinishedDialog.close()
        }

         onNo: {
             Qt.quit();
         }
    }

    GridView
    {
        id: grid
        anchors.fill: parent

        model: GameModel

        cellHeight: gameScene.width/3
        cellWidth: cellHeight

        delegate: SceneRectangle
        {
            width: grid.cellWidth
            height: grid.cellHeight
        }
    }

}
