import QtQuick 2.0
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

Rectangle {

    id: initialScreen
    color: "#fff1ba"

    Button
    {
        id: playButton
        anchors.centerIn: parent
        width: parent.width / 5
        height: 50

        contentItem: Text {
            text: "Play!"
            color: "black"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: Text.AlignHCenter
            font.pointSize: 15
        }

        background: Rectangle
        {
            color: playButton.pressed ? "#8a93d1" : "lightblue"
            radius: height/3
        }

        onClicked: stackView.push(Qt.resolvedUrl("PlayScreen.qml"))
    }


}
