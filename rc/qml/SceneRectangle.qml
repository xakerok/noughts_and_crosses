import QtQuick 2.0

Item
{
    id: sceneItem
    Rectangle {

        color: (mouseArea.containsMouse && state == "initial") ?
                   (!GameController.currentPlayerNumber ? "#ffd1d1" : "#d1d1ff") : "#fafafa"
        id: sceneRect

        anchors.fill: parent
        anchors.topMargin: 2
        anchors.bottomMargin: 2
        anchors.leftMargin: 2
        anchors.rightMargin: 2

        states: [
            State {
                name: "initial"
                when: model.itemState === "initial"

                PropertyChanges {
                    target: sceneImage
                    source: ""
                }
            },

            State {
                name: "nought"
                when: model.itemState === "nought"
                PropertyChanges {
                    target: sceneImage
                    source: "qrc:/icons/nought.png"
                }
                PropertyChanges {
                    target: sceneRect
                    color: "red"
                }
            },

            State {
                name: "cross"
                when: model.itemState === "cross"
                PropertyChanges {
                    target: sceneImage
                    source: "qrc:/icons/cross.png"
                }
                PropertyChanges {
                    target: sceneRect
                    color: "blue"
                }
            }
        ]

        Image
        {
            id: sceneImage
            anchors.fill: parent
            fillMode: Image.PreserveAspectFit
        }

        Behavior on state {
            NumberAnimation
            {
                duration: 250
                easing.type: Easing.OutCubic
            }
        }

        MouseArea
        {
            id: mouseArea
            anchors.fill: parent
            hoverEnabled: true
            onClicked: { GameController.itemClicked( index ) }
        }

    }
}
