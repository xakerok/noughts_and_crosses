import QtQuick 2.7
import QtQuick.Controls 2.0
import QtQuick.Layouts 1.0

ApplicationWindow {
    id: appWindow
    visible: true
    width: 640
    height: 480

    minimumHeight: 300
    minimumWidth: 300
    title: qsTr("Noughts and Crosses")

    StackView
    {
        id: stackView
        anchors.fill: parent
        Component.onCompleted: push( Qt.resolvedUrl("InitialScreen.qml") )
    }

}
