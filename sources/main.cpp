#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>

#include "gamecontroller.h"

int main(int argc, char *argv[])
{
   QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
   QGuiApplication app(argc, argv);

   QQmlApplicationEngine engine;

   GameController gameController;

   engine.rootContext()->setContextProperty("GameController", &gameController );
   engine.rootContext()->setContextProperty("GameModel", gameController.model() );

   engine.load( QUrl(QLatin1String("qrc:/qml/main.qml")) );

   return app.exec();
}
