#include "gamecontroller.h"

#include <QDebug>
#include "tableitem.h"

GameController::GameController(QObject *parent) : QObject(parent)
{
   m_model = new GameTableModel( this );
   m_currentPlayerNumber = 0;
}

void GameController::itemClicked(int idx)
{
   if( m_model->data(m_model->index( idx ), GameTableModel::e_ItemStateRole).toInt() != TableItem::eInitial )
   {
      return;
   }

   m_model->setData( m_model->index(idx),
                     m_currentPlayerNumber ? TableItem::eCross : TableItem::eNought,
                     GameTableModel::e_ItemStateRole );

   checkResult();

}

QAbstractItemModel* GameController::model() const
{
   return m_model;
}

int GameController::currentPlayerNumber() const
{
   return m_currentPlayerNumber;
}

void GameController::restartGame()
{
   m_model->resetGameTable();
   m_currentPlayerNumber = 0;
   Q_EMIT currentPlayerNumberChanged();
}

void GameController::switchPlayerMove()
{
   if( !m_currentPlayerNumber )
      m_currentPlayerNumber++;
   else
      m_currentPlayerNumber--;
   Q_EMIT currentPlayerNumberChanged();
}

void GameController::checkResult()
{
   TableItem::EStates winner = m_model->checkWinner();

   if( winner == TableItem::eInitial )
   {
      if( m_model->hasMoves() )
      {
         switchPlayerMove();
      }
      else
      {
         Q_EMIT gameFinished();
      }
   }
   else
   {
      Q_EMIT winnerFound( tr("Player ") + QString::number( m_currentPlayerNumber + 1)  );
   }

}
