#include "gametablemodel.h"
#include "tableitem.h"
#include <QDebug>

const int c_gridSize = 3;
const int c_itemsCount = c_gridSize * c_gridSize;

GameTableModel::GameTableModel(QObject *parent):QAbstractListModel( parent )
{
   for( int i=0; i<c_itemsCount; i++ )
      m_items.append( new TableItem() );

   resetGameTable();
}

GameTableModel::~GameTableModel()
{
   qDeleteAll( m_items );
}

int GameTableModel::rowCount(const QModelIndex& parent) const
{
   if( parent.isValid() )
      return 0;
   return c_itemsCount;
}

QVariant GameTableModel::data(const QModelIndex& index, int role) const
{
   if( !index.isValid() )
      return QVariant();

   if( role == eItemStateNameRole )
      return m_items.at( index.row() )->getStateName();

   if( role == e_ItemStateRole )
      return m_items.at( index.row() )->getState();

   return QVariant();
}

bool GameTableModel::setData(const QModelIndex& index, const QVariant& value, int role)
{
   if( !index.isValid() )
      return false;

   if( role == e_ItemStateRole )
   {
      m_items[index.row()]->setState( static_cast<TableItem::EStates>( value.toInt() ) );
      Q_EMIT dataChanged( index, index );
      return true;
   }
   return false;
}

QHash<int, QByteArray> GameTableModel::roleNames() const
{
   QHash<int,QByteArray> hash;
   hash.insert( eItemStateNameRole, "itemState" );
   return hash;
}

bool GameTableModel::hasMoves() const
{
   for( int i=0; i<m_items.size(); i++ )
   {
      if( m_items.at(i)->getState() == TableItem::eInitial )
         return true;
   }
   return false;
}

TableItem::EStates GameTableModel::checkWinner() const
{
   auto checkIfWin = [this]( const TableItem::EStates state)
   {
      for( int i=0; i < c_gridSize; i++ )
      {
         if( m_items[i]->getState() == state &&
             m_items[i+c_gridSize]->getState() == state &&
             m_items[i+2*c_gridSize]->getState() == state )
            return true;

         if( m_items[i*c_gridSize]->getState() == state &&
             m_items[i*c_gridSize+1]->getState() == state &&
             m_items[i*c_gridSize+2]->getState() == state )
            return true;
      }

      bool mainDiagonal = true;
      bool sideDiagonal = true;

      for( int i = 0; i < c_gridSize; i++)
      {
         for (int j = 0; j < c_gridSize; j++)
         {
            if( i == j )
            {
               if( m_items[i*c_gridSize + j]->getState() != state )
               {
                  mainDiagonal = false;
                  break;
               }
            }
         }
         if( !mainDiagonal )
            break;
      }

      if( mainDiagonal )
         return true;

      for( int i = 0; i < c_gridSize; i++)
      {
         for (int j = 0; j < c_gridSize; j++)
         {
            if( (i + j) == (c_gridSize - 1) )
            {
               if( m_items[i*c_gridSize + j]->getState() != state )
               {
                  sideDiagonal = false;
                  break;
               }
            }
         }
         if( !sideDiagonal )
            break;
      }

      if( sideDiagonal )
         return true;

   return false;

   };

   if( checkIfWin( TableItem::eCross ) )
      return TableItem::eCross;

   if( checkIfWin( TableItem::eNought ) )
      return TableItem::eNought;

   return TableItem::eInitial;

}

void GameTableModel::resetGameTable()
{
   beginResetModel();
   for( int i=0; i < m_items.size(); i++ )
   {
      m_items[i]->resetState();
   }
   endResetModel();
}
