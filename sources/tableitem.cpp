#include "tableitem.h"

TableItem::TableItem() :
   m_state( eInitial )
{
}

TableItem::~TableItem()
{

}

void TableItem::resetState()
{
   m_state = eInitial;
}

void TableItem::setState(TableItem::EStates state)
{
   m_state = state;
}

TableItem::EStates TableItem::getState() const
{
   return m_state;
}

QString TableItem::getStateName() const
{
   switch (m_state) {
      case eInitial:
         return "initial";
         break;
      case eCross:
         return "cross";
      case eNought:
         return "nought";
      default:
         break;
   }
   // incorrect state
   Q_ASSERT( false );
   return QString();
}
