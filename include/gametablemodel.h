#ifndef GAMETABLEMODEL_H
#define GAMETABLEMODEL_H

#include <QAbstractTableModel>

#include "tableitem.h"

class GameTableModel : public QAbstractListModel
{
public:
   explicit GameTableModel(QObject* parent = nullptr);
   virtual ~GameTableModel();

   enum EGameTableModelStates
   {
      eItemStateNameRole = Qt::UserRole + 1,
      e_ItemStateRole
   };

   int rowCount(const QModelIndex &parent) const;
   QVariant data(const QModelIndex &index, int role) const;
   bool setData(const QModelIndex &index, const QVariant &value, int role);

   QHash<int, QByteArray> roleNames() const;

   TableItem::EStates checkWinner() const;

   bool hasMoves() const;

   void resetGameTable();

private:
   QVector<TableItem*> m_items;

};

#endif // GAMETABLEMODEL_H
