#ifndef TABLEITEM_H
#define TABLEITEM_H

#include <QString>

class TableItem
{
public:
   explicit TableItem();
   ~TableItem();

   typedef enum
   {
      eInitial = 0,
      eNought = 1,
      eCross = 2
   } EStates;

   void resetState();
   void setState( EStates state );

   EStates getState() const;
   QString getStateName() const;

private:
   EStates m_state;

};

#endif // TABLEITEM_H
