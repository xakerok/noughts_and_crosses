#ifndef GAMECONTROLLER_H
#define GAMECONTROLLER_H

#include <QObject>
#include "gametablemodel.h"

class GameController : public QObject
{
   Q_OBJECT
   Q_PROPERTY(int currentPlayerNumber READ currentPlayerNumber NOTIFY currentPlayerNumberChanged)
public:
   explicit GameController(QObject *parent = 0);

   Q_INVOKABLE void itemClicked(int index);
   Q_INVOKABLE void restartGame();

   QAbstractItemModel* model() const;

   int currentPlayerNumber() const;

Q_SIGNALS:
   void winnerFound( const QString& playerName );
   void gameFinished();
   void currentPlayerNumberChanged();
protected:
   void switchPlayerMove();
   void checkResult();

private:
   GameTableModel* m_model;
   int m_currentPlayerNumber;
};

#endif // GAMECONTROLLER_H
